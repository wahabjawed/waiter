package com.availdo.orders;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import com.availdo.orders.util.SQLHelper;


/**
 * Created by Abdul Wahab on 8/10/2015.
 */
public class DreamApp extends Application {

    public static SQLiteDatabase db;
    public static Context context;
    public static SharedPreferences prefs;
    public static SharedPreferences.Editor editor;

    public static SQLiteDatabase getDb() {
        return db;
    }

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {

        // TODO Auto-generated method stub
        super.onCreate();
        db = openOrCreateDatabase("DreamApp", MODE_PRIVATE, null);
        context = getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        SQLHelper.setupDB();
    }

    @Override
    public void onLowMemory() {
        // TODO Auto-generated method stub
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        // TODO Auto-generated method stub
        super.onTerminate();
        db.close();
    }

}
