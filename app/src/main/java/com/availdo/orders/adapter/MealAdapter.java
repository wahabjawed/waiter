package com.availdo.orders.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.availdo.orders.R;
import com.availdo.orders.model.MealListObj;

import java.util.ArrayList;
import java.util.List;


public class MealAdapter extends ArrayAdapter<MealListObj> {

    private FragmentActivity activity;
    private List<MealListObj> data= new ArrayList<>();

    public MealAdapter(FragmentActivity context,List<MealListObj> _data) {
        super(context, R.layout.row_order_list, _data);
        this.activity = context;
        this.data = _data;
    }

    public void remove(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        public TextView name;
        public TextView date;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        //final int pos = position;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Log.d("DreamApp--GoalAdapter", "Inflating Layout");
            vi = inflater.inflate(R.layout.row_order_list, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) vi.findViewById(R.id.name);
            viewHolder.date = (TextView) vi.findViewById(R.id.date);
            vi.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) vi.getTag();

        MealListObj obj = data.get(position);
        if (obj != null) {
            holder.name.setText(obj.getName());
            holder.date.setText(obj.getDate());
        }
        return vi;
    }
}