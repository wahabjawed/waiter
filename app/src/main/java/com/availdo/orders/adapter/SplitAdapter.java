package com.availdo.orders.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.availdo.orders.R;
import com.availdo.orders.fragment.NewMealFragment;
import com.availdo.orders.model.CustomerObj;

import java.util.List;


public class SplitAdapter extends ArrayAdapter<CustomerObj> {

    private Activity activity;
    private List<CustomerObj> data;
    private double charges;

    private double chargesOneOff;

    public SplitAdapter(Activity context, List<CustomerObj> _data) {
        super(context, R.layout.row_split, _data);
        this.activity = context;
        this.data = _data;

        if (NewMealFragment.restaurantObj.getExtraItems().size() > 0)
            charges = NewMealFragment.restaurantObj.getCharges() / data.size();

        if (NewMealFragment.restaurantObj.getChargesOne() > 0)
            chargesOneOff = NewMealFragment.restaurantObj.getChargesOne() / data.size();
    }

    static class ViewHolder {
        public TextView name;
        public TextView amount;
        public TextView chargesShared;
        public TextView chargesOneOffL;
        public TextView chargesOneOff;
        public TextView chargesPCharges;
        public TextView chargesPChargesL;
        public TextView total;

        public LinearLayout chargesLayout;
        public LinearLayout chargesOneLayout;
        public LinearLayout chargesPLayout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        //final int pos = position;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.row_split, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) vi.findViewById(R.id.label2);
            viewHolder.amount = (TextView) vi.findViewById(R.id.label3);
            viewHolder.chargesShared = (TextView) vi.findViewById(R.id.label5);
            viewHolder.chargesOneOffL = (TextView) vi.findViewById(R.id.labelOneCharge);
            viewHolder.chargesOneOff = (TextView) vi.findViewById(R.id.OneCharge);
            viewHolder.chargesPChargesL = (TextView) vi.findViewById(R.id.labelPCharge);
            viewHolder.chargesPCharges = (TextView) vi.findViewById(R.id.PCharge);
            viewHolder.total = (TextView) vi.findViewById(R.id.label6);

            viewHolder.chargesLayout = (LinearLayout) vi.findViewById(R.id.sharedLayout);
            viewHolder.chargesOneLayout = (LinearLayout) vi.findViewById(R.id.oneChargeLayout);
            viewHolder.chargesPLayout = (LinearLayout) vi.findViewById(R.id.perChargeLayout);

            vi.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) vi.getTag();

        CustomerObj obj = data.get(position);
        if (obj != null) {
            holder.name.setText(obj.getName());
            holder.amount.setText("$" + String.format("%.2f",obj.getTotal()));

            if (charges > 0) {
                holder.chargesLayout.setVisibility(View.VISIBLE);
                holder.chargesShared.setText("$" + String.format("%.2f",(charges)));
            } else {
                holder.chargesLayout.setVisibility(View.GONE);
            }

            if (chargesOneOff > 0) {
                holder.chargesOneLayout.setVisibility(View.VISIBLE);
                holder.chargesOneOffL.setText(NewMealFragment.restaurantObj.getChargesOneL());
                holder.chargesOneOff.setText("$" + String.format("%.2f",(chargesOneOff)));

            } else {
                holder.chargesOneLayout.setVisibility(View.GONE);
            }
            double chargesPer = 0;
            if (NewMealFragment.restaurantObj.getChargesPer() > 0) {
                holder.chargesPLayout.setVisibility(View.VISIBLE);
                holder.chargesPChargesL.setText(NewMealFragment.restaurantObj.getChargesPerL());
                chargesPer = ((obj.getTotal() + charges) * NewMealFragment.restaurantObj.getChargesPer()) / 100;
                holder.chargesPCharges.setText("$" + String.format("%.2f",(chargesPer)));

            } else {
                holder.chargesPLayout.setVisibility(View.GONE);
            }


            holder.total.setText("$" + String.format("%.2f",(chargesOneOff + charges + obj.getTotal() + chargesPer)));
        }

        return vi;
    }
}