package com.availdo.orders.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.availdo.orders.R;
import com.availdo.orders.model.SummaryObj;

import java.util.List;


public class SummaryAdapter extends ArrayAdapter<SummaryObj> {

    private Activity activity;
    private List<SummaryObj> data;

    public SummaryAdapter(Activity context, List<SummaryObj> _data) {
        super(context, R.layout.row_summary, _data);
        this.activity = context;
        this.data = _data;
    }

    static class ViewHolder {
        public TextView name;
        public TextView amount;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        //final int pos = position;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.row_summary, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) vi.findViewById(R.id.label1);
            viewHolder.amount = (TextView) vi.findViewById(R.id.label3);
            vi.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) vi.getTag();

        SummaryObj obj = data.get(position);
        if (obj != null) {
            holder.name.setText(obj.getQuantity()+" x "+obj.getParticular());
            holder.amount.setText("$"+String.format("%.2f",obj.getPrice()));
        }
        return vi;
    }
}