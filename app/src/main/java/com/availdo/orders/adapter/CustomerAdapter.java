package com.availdo.orders.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.availdo.orders.R;
import com.availdo.orders.model.CustomerItemObj;
import com.availdo.orders.model.CustomerObj;

import java.util.List;


public class CustomerAdapter extends ArrayAdapter<CustomerObj> {

    private Activity activity;
    private List<CustomerObj> data;

    public CustomerAdapter(Activity context, List<CustomerObj> _data) {
        super(context, R.layout.row_customer_list, _data);
        this.activity = context;
        this.data = _data;
    }

    public void remove(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        public TextView name;
        public TextView amount;
        public TextView itemList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        //final int pos = position;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Log.d("DreamApp--GoalAdapter", "Inflating Layout");
            vi = inflater.inflate(R.layout.row_customer_list, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) vi.findViewById(R.id.name);
            viewHolder.amount = (TextView) vi.findViewById(R.id.amount);
            viewHolder.itemList = (TextView) vi.findViewById(R.id.itemList);
            vi.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) vi.getTag();

        CustomerObj obj = data.get(position);
        if (obj != null) {
            holder.name.setText(obj.getName());
            holder.amount.setText("$" + String.format("%.2f", obj.getTotal()));
            String itemText = "";
            for (CustomerItemObj item : obj.getItems()) {
                itemText += item.getName() + ", ";
            }
            if (itemText.length() > 2) {
                itemText = itemText.substring(0, itemText.length() - 2);
            }
            obj.setItemList(itemText);

            holder.itemList.setText(itemText);
        }
        return vi;
    }
}