package com.availdo.orders.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.availdo.orders.R;
import com.availdo.orders.model.CustomerItemObj;

import java.util.List;


public class ItemAdapter extends ArrayAdapter<CustomerItemObj> {

    private Activity activity;
    private List<CustomerItemObj> data;

    public ItemAdapter(Activity context, List<CustomerItemObj> _data) {
        super(context, R.layout.row_item_list, _data);
        this.activity = context;
        this.data = _data;
    }

    public void remove(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        public TextView name;
        public TextView item;
        public TextView amount;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        //final int pos = position;
        if (vi == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.row_item_list, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) vi.findViewById(R.id.name);
            viewHolder.item = (TextView) vi.findViewById(R.id.item);
            viewHolder.amount = (TextView) vi.findViewById(R.id.amount);
            vi.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) vi.getTag();

        CustomerItemObj obj = data.get(position);
        if (obj != null) {
            holder.item.setText(obj.getItem());
            holder.name.setText(obj.getName());
            holder.amount.setText("$" + String.format("%.2f",obj.getAmount()));
        }
        return vi;
    }
}