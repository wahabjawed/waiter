package com.availdo.orders.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.availdo.orders.DreamApp;
import com.availdo.orders.R;
import com.availdo.orders.util.SQLHelper;

/**
 * Created by Availdo on 11/13/2015.
 */
public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        SQLHelper.setupDB();
        DreamApp.editor.putBoolean("first_time", true);
        DreamApp.editor.commit();
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                Intent i = new Intent(Splash.this, Dashboard.class);
                startActivity(i);
                Splash.this.finish();
            }

        };
        handler.postDelayed(r, 1000 * 1);
    }

}
