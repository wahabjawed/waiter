package com.availdo.orders.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.availdo.orders.R;
import com.availdo.orders.activity.Dashboard;
import com.availdo.orders.adapter.CustomerAdapter;
import com.availdo.orders.model.CustomerObj;
import com.availdo.orders.model.RestaurantObj;
import com.availdo.orders.util.SQLHelper;
import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewMealFragment extends Fragment {

    @Bind(R.id.restaurant)
    EditText restaurant;
    @Bind(R.id.add)
    TextView add;
    @Bind(R.id.customerList)
    ListView customerList;
    @Bind(R.id.subtotalL)
    TextView subtotalL;
    @Bind(R.id.subtotal)
    TextView subtotal;
    @Bind(R.id.totalL)
    TextView totalL;
    @Bind(R.id.total)
    TextView total;
    @Bind(R.id.summary)
    Button summary;
    @Bind(R.id.split)
    Button split;

    double totalAmount;

    public static RestaurantObj restaurantObj;
    @Bind(R.id.create)
    Button create;
    @Bind(R.id.extra)
    Button extra;
    @Bind(R.id.chargesOneL)
    TextView chargesOneL;
    @Bind(R.id.chargesOne)
    TextView chargesOne;
    @Bind(R.id.layoutChargesOne)
    LinearLayout layoutChargesOne;
    @Bind(R.id.chargesPerL)
    TextView chargesPerL;
    @Bind(R.id.chargesPer)
    TextView chargesPer;
    @Bind(R.id.layoutChargesPer)
    LinearLayout layoutChargesPer;

    private AlphaInAnimationAdapter mAnimAdapter;
    CustomerAdapter adapter;

    int updateID = -1;

    List<CustomerObj> objList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        setHasOptionsMenu(true);

        if (bundle.getInt("isUpdate") == 1) {
            updateID = bundle.getInt("MealID");
            restaurantObj = SQLHelper.getOrder(updateID);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ((Dashboard) getActivity())
                .mTitle.setText("NEW ORDER");
        ((Dashboard) getActivity())
                .getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        objList = new ArrayList<>();
        objList.addAll(restaurantObj.getCustomer());
        if (restaurantObj.getExtraItems().size() > 0) {
            objList.add(new CustomerObj("Shared Items", restaurantObj.getCharges(), restaurantObj.getExtraItems()));
        }


        adapter = new CustomerAdapter(getActivity(), objList);
        mAnimAdapter = new AlphaInAnimationAdapter(adapter);
        mAnimAdapter.setAbsListView(customerList);
        customerList.setAdapter(mAnimAdapter);


        if (restaurantObj.getChargesPer() > 0) {
            layoutChargesPer.setVisibility(View.VISIBLE);
            chargesPer.setText(String.format("%.2f", restaurantObj.getChargesPer()) + "%");
            chargesPerL.setText(restaurantObj.getChargesPerL());
        } else {
            layoutChargesPer.setVisibility(View.GONE);
        }

        if (restaurantObj.getChargesOne() > 0) {
            layoutChargesOne.setVisibility(View.VISIBLE);
            chargesOne.setText("$" + String.format("%.2f", restaurantObj.getChargesOne()));
            chargesOneL.setText(restaurantObj.getChargesOneL());
        } else {
            layoutChargesOne.setVisibility(View.GONE);
        }


        calcAmount();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_newmeal, container, false);

        ButterKnife.bind(this, rootView);

        restaurant.setText(restaurantObj.getName());

        split.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (restaurantObj.getCustomer().size() > 1) {
                    restaurantObj.setSubTotal(totalAmount);
                    Fragment fragment = new SplitFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();
                } else {
                    Toast.makeText(NewMealFragment.this.getActivity(), "No Order To Split", Toast.LENGTH_LONG).show();
                }
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(restaurant.getText().toString())) {
                    Toast.makeText(NewMealFragment.this.getActivity(), "Kindly fill the Restaurant Name", Toast.LENGTH_LONG).show();
                    return;
                }

                if (restaurantObj.getCustomer().size() == 0) {
                    Toast.makeText(NewMealFragment.this.getActivity(), "Kindly Add Customers to Create Order", Toast.LENGTH_LONG).show();
                    return;
                }

                restaurantObj.setName(restaurant.getText().toString());
                restaurantObj.setTotal(totalAmount);
                restaurantObj.setSubTotal(totalAmount);

                if (updateID == -1)
                    SQLHelper.CreateOrder(restaurantObj);
                else
                    SQLHelper.UpdateOrder(updateID, restaurantObj);

                Toast.makeText(NewMealFragment.this.getActivity(), "Order '" + restaurantObj.getName() + "' Created", Toast.LENGTH_LONG).show();
                NewMealFragment.this.getActivity().onBackPressed();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new NewCustomerFragment();
                Bundle b = new Bundle();
                b.putBoolean("isUpdate", false);
                fragment.setArguments(b);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();

            }
        });

        summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new SummaryFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();

            }
        });

        registerForContextMenu(customerList);

        restaurant.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (restaurant.getText().toString().toLowerCase().contains("order"))
                    restaurant.setText("");
                return false;
            }
        });

        extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new ExtraFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();
            }
        });


        final SwipeToDismissTouchListener<ListViewAdapter> touchListener =
                new SwipeToDismissTouchListener<>(
                        new ListViewAdapter(customerList),
                        new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                            @Override
                            public boolean canDismiss(int position) {
                                return (!objList.get(position).getName().equals("Shared Items"));
                            }


                            public void onPendingDismiss(ListViewAdapter recyclerView, int position) {

                            }

                            @Override
                            public void onDismiss(ListViewAdapter view, int position) {
                                adapter.remove(position);
                                restaurantObj.getCustomer().remove(position);
                                calcAmount();
                            }
                        });

        touchListener.setDismissDelay(500);

        customerList.setOnTouchListener(touchListener);
        customerList.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());
        customerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (touchListener.existPendingDismisses()) {
                    touchListener.undoPendingDismiss();
                } else {

                    if (!objList.get(position).getName().equals("Shared Items")) {
                        Fragment fragment = new NewCustomerFragment();
                        Bundle b = new Bundle();
                        b.putBoolean("isUpdate", true);
                        b.putInt("ID", position);
                        fragment.setArguments(b);
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.replace(R.id.container_body, fragment);
                        fragmentTransaction.commit();
                    }
                }
            }
        });

        return rootView;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        return;
    }


    private void calcAmount() {
        totalAmount = 0;
        for (int i = 0; i < restaurantObj.getCustomer().size(); i++) {
            totalAmount += restaurantObj.getCustomer().get(i).getTotal();
        }
        totalAmount += restaurantObj.getCharges();
        subtotal.setText("$" + String.format("%.2f", totalAmount));
        total.setText("$" + String.format("%.2f", (totalAmount + restaurantObj.getChargesOne() + (((totalAmount) * restaurantObj.getChargesPer()) / 100))));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.share:

//                File file = null;
//                File root = Environment.getExternalStorageDirectory();
//                if (root.canWrite()) {
//                    File dir = new File(root.getAbsolutePath() + "/PersonData");
//                    dir.mkdirs();
//                    file = new File(dir, "Data.html");
//                    FileOutputStream out = null;
//                    try {
//                        out = new FileOutputStream(file);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        out.write(getEmailBody().getBytes());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        out.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                Uri u1 = null;
//                u1 = Uri.fromFile(file);
//

                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("text/html");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{""});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Orders Android App");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(getEmailBody()));
                Log.e("Print Material", Html.fromHtml(getEmailBody()).toString());
                // emailIntent.putExtra(Intent.EXTRA_STREAM, u1);
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private String spaces(int numberOfSpaces) {
        //String builder is efficient at concatenating strings together
        StringBuilder sb = new StringBuilder();

        //Loop as many times as specified; each time add a space to the string
        for (int i = 0; i < numberOfSpaces; i++) {
            sb.append("-");
        }

        //Return the string
        return sb.toString();
    }

    /**
     * @return hello world
     */
    @NonNull
    private String getEmailBody() {
        StringBuilder builder = new StringBuilder();


        builder.append("<body><h3>" + restaurantObj.getName() + "</h3></br>");

        for (int i = 0; i < objList.size(); i++) {

            builder.append("<p>Name: " + objList.get(i).getName() + spaces(20 - objList.get(i).getName().length()) + "$" + String.format("%.2f", objList.get(i).getTotal()) + "</p>  </br></br>");
            builder.append("<p> Items: " + objList.get(i).getItemList() + "</p><p></p>");
        }


        builder.append("  <p><strong>Sub Total: </strong>" + subtotal.getText().toString() + "</p>    </br>");

        if (restaurantObj.getChargesPer() > 0) {
            builder.append(" <p><strong>" + chargesPerL.getText().toString() + ": </strong>" + chargesPer.getText().toString() + "</p>    </br>");

        }

        if (restaurantObj.getChargesOne() > 0) {
            builder.append(" <p><strong>" + chargesOneL.getText().toString() + ": </strong>" + chargesOne.getText().toString() + "</p>    </br>");

        }

        builder.append(" <p><strong>Total: </strong>" + total.getText().toString() + "</p>    </br>");
        builder.append("<p>===================================================</p>");

        builder.append("<h5>Split Bill</h5></br>");


        double charges = 0, chargesOneOff = 0;
        if (restaurantObj.getExtraItems().size() > 0)
            charges = restaurantObj.getCharges() / restaurantObj.getCustomer().size();

        if (restaurantObj.getChargesOne() > 0)
            chargesOneOff = restaurantObj.getChargesOne() / restaurantObj.getCustomer().size();

        for (int i = 0; i < restaurantObj.getCustomer().size(); i++) {
            double cost = restaurantObj.getCustomer().get(i).getTotal() + charges + chargesOneOff + (((restaurantObj.getCustomer().get(i).getTotal() + charges) * restaurantObj.getChargesPer()) / 100);

            builder.append("<p>Name: " + restaurantObj.getCustomer().get(i).getName() + spaces(20 - restaurantObj.getCustomer().get(i).getName().length()) + "$" + String.format("%.2f", cost) + "</p>  </br></br>");
        }


        builder.append("</body>");
        return builder.toString();

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuInflater mi = getActivity().getMenuInflater();
        mi.inflate(R.menu.items, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}