package com.availdo.orders.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.availdo.orders.R;
import com.availdo.orders.activity.Dashboard;
import com.availdo.orders.adapter.MealAdapter;
import com.availdo.orders.model.MealListObj;
import com.availdo.orders.model.RestaurantObj;
import com.availdo.orders.util.SQLHelper;
import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MealFragment extends Fragment {


    @Bind(R.id.meal_list)
    ListView mealList;
    @Bind(R.id.emptyElement)
    TextView emptyElement;
    @Bind(R.id.done)
    Button done;
    private AlphaInAnimationAdapter mAnimAdapter;
    List<MealListObj> objs = new ArrayList<>();
    MealAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((Dashboard) getActivity())
                .mTitle.setText("ORDERS");
        ((Dashboard) getActivity())
                .getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_meals, container, false);

        ButterKnife.bind(this, rootView);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewMealFragment.restaurantObj = new RestaurantObj();
                NewMealFragment.restaurantObj.setName("Order " + (objs.size() + 1));
                Fragment fragment = new NewMealFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("isUpdate", 0);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();
            }
        });

        mealList.setDivider(null);
        objs = SQLHelper.getMeals();
        adapter = new MealAdapter(getActivity(), objs);
        mAnimAdapter = new AlphaInAnimationAdapter(adapter);
        mAnimAdapter.setAbsListView(mealList);
        mealList.setAdapter(mAnimAdapter);
        mealList.setEmptyView(rootView.findViewById(R.id.emptyElement));

        final SwipeToDismissTouchListener<ListViewAdapter> touchListener =
                new SwipeToDismissTouchListener<>(
                        new ListViewAdapter(mealList),
                        new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }


                            public void onPendingDismiss(ListViewAdapter recyclerView, int position) {

                            }

                            @Override
                            public void onDismiss(ListViewAdapter view, int position) {
                                SQLHelper.DeleteMeal(objs.get(position).getId());
                                adapter.remove(position);
                            }
                        });

        touchListener.setDismissDelay(500);

        mealList.setOnTouchListener(touchListener);
        mealList.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());
        mealList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (touchListener.existPendingDismisses()) {
                    touchListener.undoPendingDismiss();
                } else {
                    Fragment fragment = new NewMealFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("isUpdate", 1);
                    bundle.putInt("MealID", objs.get(position).getId());
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();
                }
            }
        });


        // Inflate the layout for this fragment

        return rootView;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        return;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}