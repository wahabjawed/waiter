package com.availdo.orders.fragment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.availdo.orders.R;
import com.availdo.orders.activity.Dashboard;
import com.availdo.orders.adapter.SummaryAdapter;
import com.availdo.orders.model.CustomerObj;
import com.availdo.orders.model.SummaryObj;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class SummaryFragment extends Fragment {


    @Bind(R.id.label1)
    TextView label1;
    @Bind(R.id.label2)
    TextView label2;
    @Bind(R.id.meal_list)
    ListView summaryList;
    @Bind(R.id.emptyElement)
    TextView emptyElement;
    private List<SummaryObj> objs = new ArrayList<>();
    private AlphaInAnimationAdapter mAnimAdapter;
    SummaryAdapter adapter;
    String mPath;

    public SummaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((Dashboard) getActivity()).mTitle.setText("SUMMARY");
        ((Dashboard) getActivity())
                .getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((Dashboard) getActivity()).mTitle.setText("SUMMARY");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_summary, container, false);

        ButterKnife.bind(this, rootView);
        summaryList.setDivider(null);

        for(int i =0;i<NewMealFragment.restaurantObj.getCustomer().size();i++){
            CustomerObj obj = NewMealFragment.restaurantObj.getCustomer().get(i);
            for(int j=0;j<obj.getItems().size();j++){
                boolean isPresent =false;
                for (int  k =0; k<objs.size();k++ ) {
                    if(obj.getItems().get(j).getName().trim().toLowerCase().equals(objs.get(k).getParticular().trim().toLowerCase())){
                        objs.get(k).setQuantity((objs.get(k).getQuantity()+1));
                        objs.get(k).setPrice((objs.get(k).getPrice()+obj.getItems().get(j).getAmount()));
                        isPresent=true;
                        break;
                    }
                }
                if(!isPresent) {
                    objs.add(new SummaryObj(obj.getItems().get(j).getName(), obj.getItems().get(j).getAmount(), 1));
                }
            }

        }

        for(int j=0;j< NewMealFragment.restaurantObj.getExtraItems().size();j++){
            boolean isPresent =false;
            for (int  k =0; k<objs.size();k++ ) {
                if(NewMealFragment.restaurantObj.getExtraItems().get(j).getName().trim().toLowerCase().equals(objs.get(k).getParticular().trim().toLowerCase())){
                    objs.get(k).setQuantity((objs.get(k).getQuantity()+1));
                    objs.get(k).setPrice((objs.get(k).getPrice()+NewMealFragment.restaurantObj.getExtraItems().get(j).getAmount()));
                    isPresent=true;
                    break;
                }
            }
            if(!isPresent) {
                objs.add(new SummaryObj(NewMealFragment.restaurantObj.getExtraItems().get(j).getName(), NewMealFragment.restaurantObj.getExtraItems().get(j).getAmount(), 1));
            }
        }


        adapter = new SummaryAdapter(getActivity(), objs);
        mAnimAdapter = new AlphaInAnimationAdapter(adapter);
        mAnimAdapter.setAbsListView(summaryList);
        summaryList.setAdapter(mAnimAdapter);
        summaryList.setEmptyView(rootView.findViewById(R.id.emptyElement));

        return rootView;
    }


    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            mPath = SummaryFragment.this.getActivity().getFilesDir()+File.separator + now + ".jpg";

            // create bitmap screen capture
            View v1 = getActivity().getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

           openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }


    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
