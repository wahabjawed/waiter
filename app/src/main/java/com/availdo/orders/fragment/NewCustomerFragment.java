package com.availdo.orders.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.availdo.orders.R;
import com.availdo.orders.activity.Dashboard;
import com.availdo.orders.adapter.ItemAdapter;
import com.availdo.orders.model.CustomerItemObj;
import com.availdo.orders.model.CustomerObj;
import com.availdo.orders.util.SQLHelper;
import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewCustomerFragment extends Fragment {


    @Bind(R.id.customer)
    AutoCompleteTextView customer;
    @Bind(R.id.add)
    Button add;
    @Bind(R.id.customerList)
    ListView customerList;
    @Bind(R.id.totalL)
    TextView totalL;
    @Bind(R.id.total)
    TextView total;
    @Bind(R.id.done)
    Button done;
    @Bind(R.id.item)
    AutoCompleteTextView item;
    @Bind(R.id.name)
    AutoCompleteTextView name;

    private AlphaInAnimationAdapter mAnimAdapter;
    ItemAdapter adapter;
    double totalAmount = 0;

    List<CustomerItemObj> objs = new ArrayList<>();
    @Bind(R.id.price)
    EditText price;

    boolean isUpdate = false;
    int ID = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        ((Dashboard) getActivity())
                .mTitle.setText("ADD CUSTOMER");
        ((Dashboard) getActivity())
                .getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        customer.setText("Customer " + (NewMealFragment.restaurantObj.getCustomer().size() + 1));

        isUpdate = getArguments().getBoolean("isUpdate");
        if (isUpdate) {
            ID = getArguments().getInt("ID");
            objs.addAll(NewMealFragment.restaurantObj.getCustomer().get(ID).getItems());
            customer.setText(NewMealFragment.restaurantObj.getCustomer().get(ID).getName());
            totalAmount = NewMealFragment.restaurantObj.getCustomer().get(ID).getTotal();
            total.setText("$" + totalAmount);
        }

        adapter = new ItemAdapter(getActivity(), objs);
        mAnimAdapter = new AlphaInAnimationAdapter(adapter);
        mAnimAdapter.setAbsListView(customerList);
        customerList.setAdapter(mAnimAdapter);


        item.setText("Item " + (objs.size() + 1));

        ArrayAdapter<String> adapterItem = new ArrayAdapter<String>(NewCustomerFragment.this.getActivity(), android.R.layout.simple_list_item_1, SQLHelper.getItem());
        item.setAdapter(adapterItem);

        ArrayAdapter<String> adapterName = new ArrayAdapter<String>(NewCustomerFragment.this.getActivity(), android.R.layout.simple_list_item_1, SQLHelper.getName());
        name.setAdapter(adapterName);

        ArrayAdapter<String> adapterCustomer = new ArrayAdapter<String>(NewCustomerFragment.this.getActivity(), android.R.layout.simple_list_item_1, SQLHelper.getCustomer());
        customer.setAdapter(adapterCustomer);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer, container, false);

        ButterKnife.bind(this, rootView);

        item.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (item.getText().toString().toLowerCase().contains("item"))
                    item.setText("");
                return false;
            }
        });

        customer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (customer.getText().toString().toLowerCase().contains("customer"))
                    customer.setText("");
                return false;
            }
        });

        customerList.setDivider(null);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(name.getText().toString())) {
                    Toast.makeText(NewCustomerFragment.this.getActivity(), "Kindly Type Item Name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(price.getText().toString())) {
                    price.setText("0");
                }

                objs.add(new CustomerItemObj(objs.size() + 1, name.getText().toString(), Double.parseDouble(price.getText().toString()), item.getText().toString()));
                adapter = new ItemAdapter(getActivity(), objs);
                mAnimAdapter = new AlphaInAnimationAdapter(adapter);
                mAnimAdapter.setAbsListView(customerList);
                customerList.setAdapter(mAnimAdapter);
                calcAmount();

                name.setText("");
                price.setText("");
                item.setText("Item " + (objs.size() + 1));
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(customer.getText().toString())) {
                    Toast.makeText(NewCustomerFragment.this.getActivity(), "Kindly Type Customer Name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (objs.size() == 0) {
                    Toast.makeText(NewCustomerFragment.this.getActivity(), "Kindly Add Atleast One Item", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isUpdate) {
                    NewMealFragment.restaurantObj.getCustomer().set(ID, new CustomerObj(customer.getText().toString(), totalAmount, objs));

                } else {
                    NewMealFragment.restaurantObj.getCustomer().add(new CustomerObj(customer.getText().toString(), totalAmount, objs));
                }
                NewCustomerFragment.this.getActivity().onBackPressed();
            }
        });


        final SwipeToDismissTouchListener<ListViewAdapter> touchListener =
                new SwipeToDismissTouchListener<>(
                        new ListViewAdapter(customerList),
                        new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }


                            public void onPendingDismiss(ListViewAdapter recyclerView, int position) {

                            }

                            @Override
                            public void onDismiss(ListViewAdapter view, int position) {
                                adapter.remove(position);
                                calcAmount();

                            }
                        });

        touchListener.setDismissDelay(500);

        customerList.setOnTouchListener(touchListener);
        customerList.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());
        customerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (touchListener.existPendingDismisses()) {
                    touchListener.undoPendingDismiss();
                } else {

                }
            }
        });

        return rootView;
    }

    private void calcAmount() {
        totalAmount = 0;
        for (int i = 0; i < objs.size(); i++) {
            totalAmount += objs.get(i).getAmount();
        }
        total.setText("$" + String.format("%.2f", totalAmount));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        return;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        ((Dashboard) getActivity()).backPress = null;
    }


}