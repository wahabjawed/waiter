package com.availdo.orders.fragment;

/**
 * Created by abdul.wahab on 7/21/2016.
 */
public interface IBackPress {

    public void onBackPress();
}
