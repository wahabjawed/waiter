package com.availdo.orders.fragment;

/**
 * Created by Availdo on 11/12/2015.
 */


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.availdo.orders.R;
import com.availdo.orders.activity.Dashboard;
import com.availdo.orders.model.CustomerObj;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;


public class ChartFragment extends Fragment {

    PieChart mChart;

    public ChartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chart, container, false);

        ((Dashboard) getActivity()).mTitle.setText("SPLIT CHART");
        ((Dashboard) getActivity())
                .getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mChart = (PieChart) rootView.findViewById(R.id.charts);
        mChart.animateXY(1400, 1400);
        mChart.setUsePercentValues(true);
        mChart.setDescription("");
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);


        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColorTransparent(true);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        mChart.setDrawSliceText(false);
        mChart.setData(generatePieData());
        // Inflate the layout for this fragment
        return rootView;
    }

    private SpannableString generateCenterSpannableText(String goal) {

        SpannableString s = new SpannableString(goal);
        s.setSpan(new RelativeSizeSpan(1.7f), 0, goal.length(), 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 0, goal.length(), 0);
        return s;
    }

    private PieData generatePieData() {

        String name = NewMealFragment.restaurantObj.getName()==null?"N/A":NewMealFragment.restaurantObj.getName();
        mChart.setCenterText(generateCenterSpannableText(name));

        ArrayList<Entry> entries1 = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < NewMealFragment.restaurantObj.getCustomer().size(); i++) {
            CustomerObj obj = NewMealFragment.restaurantObj.getCustomer().get(i);

            entries1.add(new Entry((float) (obj.getTotal() / NewMealFragment.restaurantObj.getSubTotal()) * 100, i));
            xVals.add(obj.getName());
        }

        PieDataSet ds1 = new PieDataSet(entries1, "");
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.WHITE);
        ds1.setValueTextSize(12f);
        ds1.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData d = new PieData(xVals, ds1);

        return d;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


}