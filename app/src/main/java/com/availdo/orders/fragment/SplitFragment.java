package com.availdo.orders.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.availdo.orders.R;
import com.availdo.orders.activity.Dashboard;
import com.availdo.orders.adapter.SplitAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;


public class SplitFragment extends Fragment {


    @Bind(R.id.split_list)
    ListView summaryList;
    @Bind(R.id.emptyElement)
    TextView emptyElement;
    private AlphaInAnimationAdapter mAnimAdapter;
    SplitAdapter adapter;

    public SplitFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((Dashboard) getActivity()).mTitle.setText("SPLIT CHARGES");
        ((Dashboard) getActivity())
                .getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Dashboard) getActivity()).mTitle.setText("SPLIT CHARGES");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_split, container, false);

        ButterKnife.bind(this, rootView);
        summaryList.setDivider(null);

        adapter = new SplitAdapter(getActivity(), NewMealFragment.restaurantObj.getCustomer());
        mAnimAdapter = new AlphaInAnimationAdapter(adapter);
        mAnimAdapter.setAbsListView(summaryList);
        summaryList.setAdapter(mAnimAdapter);
        summaryList.setEmptyView(rootView.findViewById(R.id.emptyElement));

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.chart:
                Fragment fragment = new ChartFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuInflater mi = getActivity().getMenuInflater();
        mi.inflate(R.menu.chart, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
