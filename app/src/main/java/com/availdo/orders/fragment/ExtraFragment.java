package com.availdo.orders.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.availdo.orders.R;
import com.availdo.orders.activity.Dashboard;
import com.availdo.orders.adapter.ItemAdapter;
import com.availdo.orders.model.CustomerItemObj;
import com.availdo.orders.util.SQLHelper;
import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ExtraFragment extends Fragment {


    @Bind(R.id.item)
    AutoCompleteTextView item;
    @Bind(R.id.name)
    AutoCompleteTextView name;
    @Bind(R.id.add)
    Button add;
    @Bind(R.id.customerList)
    ListView customerList;
    @Bind(R.id.totalL)
    TextView totalL;
    @Bind(R.id.total)
    TextView total;
    @Bind(R.id.chargesL)
    EditText chargesL;
    @Bind(R.id.charges)
    EditText charges;
    @Bind(R.id.chargespL)
    EditText chargespL;
    @Bind(R.id.charges_per)
    EditText chargesPer;
    @Bind(R.id.done)
    Button done;
    private AlphaInAnimationAdapter mAnimAdapter;
    ItemAdapter adapter;
    double totalAmount = 0;

    List<CustomerItemObj> objs = new ArrayList<>();
    @Bind(R.id.price)
    EditText price;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        ((Dashboard) getActivity())
                .mTitle.setText("ADD EXTRA");
        ((Dashboard) getActivity())
                .getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        objs = NewMealFragment.restaurantObj.getExtraItems();
        totalAmount = NewMealFragment.restaurantObj.getCharges();
        total.setText("$" + totalAmount);
        charges.setText("" + NewMealFragment.restaurantObj.getChargesOne());
        chargesPer.setText("" + NewMealFragment.restaurantObj.getChargesPer());

        if (NewMealFragment.restaurantObj.getChargesOneL() != null)
            chargesL.setText(NewMealFragment.restaurantObj.getChargesOneL());

        if (NewMealFragment.restaurantObj.getChargesPerL() != null)
            chargespL.setText(NewMealFragment.restaurantObj.getChargesPerL());

        adapter = new ItemAdapter(getActivity(), objs);
        mAnimAdapter = new AlphaInAnimationAdapter(adapter);
        mAnimAdapter.setAbsListView(customerList);
        customerList.setAdapter(mAnimAdapter);

        item.setText("Shared " + (objs.size() + 1));

        ArrayAdapter<String> adapterItem = new ArrayAdapter<String>(ExtraFragment.this.getActivity(), android.R.layout.simple_list_item_1, SQLHelper.getItem());
        item.setAdapter(adapterItem);

        ArrayAdapter<String> adapterName = new ArrayAdapter<String>(ExtraFragment.this.getActivity(), android.R.layout.simple_list_item_1, SQLHelper.getName());
        name.setAdapter(adapterName);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_extra, container, false);

        ButterKnife.bind(this, rootView);

        item.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (item.getText().toString().toLowerCase().contains("shared"))
                    item.setText("");
                return false;
            }
        });


        customerList.setDivider(null);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(name.getText().toString())) {
                    Toast.makeText(ExtraFragment.this.getActivity(), "Kindly Type Item Name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(price.getText().toString())) {
                    price.setText("0");
                }

                objs.add(new CustomerItemObj(objs.size() + 1, name.getText().toString(), Double.parseDouble(price.getText().toString()), item.getText().toString()));
                adapter = new ItemAdapter(getActivity(), objs);
                mAnimAdapter = new AlphaInAnimationAdapter(adapter);
                mAnimAdapter.setAbsListView(customerList);
                customerList.setAdapter(mAnimAdapter);
                calcAmount();

                name.setText("");
                price.setText("");
                item.setText("Shared " + (objs.size() + 1));
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(chargesPer.getText().toString())) {
                    chargesPer.setText("0");
                }
                if (TextUtils.isEmpty(charges.getText().toString())) {
                    charges.setText("0");
                }

                NewMealFragment.restaurantObj.setExtraItems(objs);
                NewMealFragment.restaurantObj.setChargesOne(Double.parseDouble(charges.getText().toString()));
                NewMealFragment.restaurantObj.setCharges(totalAmount);
                NewMealFragment.restaurantObj.setChargesPer(Double.parseDouble(chargesPer.getText().toString()));
                NewMealFragment.restaurantObj.setChargesOneL(chargesL.getText().toString());
                NewMealFragment.restaurantObj.setChargesPerL(chargespL.getText().toString());

                ExtraFragment.this.getActivity().onBackPressed();
            }
        });


        final SwipeToDismissTouchListener<ListViewAdapter> touchListener =
                new SwipeToDismissTouchListener<>(
                        new ListViewAdapter(customerList),
                        new SwipeToDismissTouchListener.DismissCallbacks<ListViewAdapter>() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }


                            public void onPendingDismiss(ListViewAdapter recyclerView, int position) {

                            }

                            @Override
                            public void onDismiss(ListViewAdapter view, int position) {
                                adapter.remove(position);
                                calcAmount();

                            }
                        });

        touchListener.setDismissDelay(500);

        customerList.setOnTouchListener(touchListener);
        customerList.setOnScrollListener((AbsListView.OnScrollListener) touchListener.makeScrollListener());
        customerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (touchListener.existPendingDismisses()) {
                    touchListener.undoPendingDismiss();
                } else {

                }
            }
        });


        return rootView;
    }

    private void calcAmount() {
        totalAmount = 0;
        for (int i = 0; i < objs.size(); i++) {
            totalAmount += objs.get(i).getAmount();
        }
        total.setText("$" + String.format("%.2f",totalAmount));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        return;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}