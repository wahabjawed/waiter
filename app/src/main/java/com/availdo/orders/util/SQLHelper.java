package com.availdo.orders.util;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.availdo.orders.DreamApp;
import com.availdo.orders.model.CustomerItemObj;
import com.availdo.orders.model.CustomerObj;
import com.availdo.orders.model.MealListObj;
import com.availdo.orders.model.RestaurantObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SQLHelper {
    public static SQLiteDatabase db = DreamApp.db;

    public static void setupDB() {
        db.execSQL("CREATE TABLE IF NOT EXISTS Orders (OrderID INTEGER PRIMARY KEY AUTOINCREMENT, ResName TEXT, SubTotal Double, Charges Double, ChargesPer Double, ChargesTotal Double, ChargesTotalL TEXT, ChargesPerL TEXT, Total Double, Date TEXT)");
        db.execSQL("CREATE TABLE IF NOT EXISTS Customers (CustomerID INTEGER PRIMARY KEY AUTOINCREMENT, OrderID INTEGER, CusName TEXT, Total Double)");
        db.execSQL("CREATE TABLE IF NOT EXISTS Items (ItemID INTEGER PRIMARY KEY AUTOINCREMENT, CustomerID INTEGER,ItemType TEXT, ItemName TEXT, Total Double, OrderID INTEGER)");
    }

    public static List<MealListObj> getMeals() {
        List<MealListObj> objList = new ArrayList<>();
        Cursor temp = db.rawQuery("select OrderID, ResName, Date from Orders order by OrderID desc", null);
        if (temp.getCount() > 0) {
            for (int i = 0; i < temp.getCount(); i++) {
                temp.moveToPosition(i);
                objList.add(new MealListObj(temp.getInt(0), temp.getString(1), temp.getString(2)));
            }
        }
        return objList;
    }

    public static RestaurantObj getOrder(int id) {
        RestaurantObj obj = new RestaurantObj();
        Cursor temp = db.rawQuery("select  ResName, SubTotal, Charges, ChargesTotal, ChargesPer, ChargesPerL, ChargesTotalL from Orders where OrderID =" + id, null);
        if (temp.moveToFirst()) {
            obj.setName(temp.getString(0));
            obj.setCharges(temp.getDouble(2));
            obj.setSubTotal(temp.getDouble(1));
            obj.setChargesOne(temp.getDouble(3));
            obj.setChargesPer(temp.getDouble(4));
            obj.setChargesPerL(temp.getString(5));
            obj.setChargesOneL(temp.getString(6));

            Cursor itemTemp = db.rawQuery("select  ItemName, Total,ItemType from Items where OrderID=" + id, null);
            List<CustomerItemObj> itemExtraList = new ArrayList<>();
            for (int j = 0; j < itemTemp.getCount(); j++) {
                itemTemp.moveToPosition(j);
                itemExtraList.add(new CustomerItemObj(0, itemTemp.getString(0), itemTemp.getDouble(1), itemTemp.getString(2)));
            }
            obj.setExtraItems(itemExtraList);

            temp = db.rawQuery("select  CustomerID, CusName, Total from Customers where OrderID =" + id, null);
            List<CustomerObj> custList = new ArrayList<>();
            for (int i = 0; i < temp.getCount(); i++) {
                temp.moveToPosition(i);
                custList.add(new CustomerObj(temp.getString(1), temp.getDouble(2), null));
                int customerID = temp.getInt(0);

                itemTemp = db.rawQuery("select  ItemName, Total,ItemType from Items where CustomerID=" + customerID, null);
                List<CustomerItemObj> itemList = new ArrayList<>();
                for (int j = 0; j < itemTemp.getCount(); j++) {
                    itemTemp.moveToPosition(j);
                    itemList.add(new CustomerItemObj(0, itemTemp.getString(0), itemTemp.getDouble(1), itemTemp.getString(2)));
                }
                itemTemp.close();
                custList.get(i).setItems(itemList);
            }
            obj.setCustomer(custList);
            temp.close();
        }
        return obj;
    }

    public static String[] getItem() {
        String[] objList = new String[0];
        Cursor temp = db.rawQuery("select DISTINCT ItemType from Items", null);
        if (temp.getCount() > 0) {
            objList = new String[temp.getCount()];
            for (int i = 0; i < temp.getCount(); i++) {
                temp.moveToPosition(i);
                objList[i] = temp.getString(0);
            }
        }
        return objList;
    }

    public static String[] getCustomer() {
        String[] objList = new String[0];
        Cursor temp = db.rawQuery("select DISTINCT CusName from Customers", null);
        if (temp.getCount() > 0) {
            objList = new String[temp.getCount()];
            for (int i = 0; i < temp.getCount(); i++) {
                temp.moveToPosition(i);
                objList[i] = temp.getString(0);
            }
        }
        return objList;
    }

    public static String[] getName() {
        String[] objList = new String[0];
        Cursor temp = db.rawQuery("select DISTINCT ItemName from Items", null);
        if (temp.getCount() > 0) {
            objList = new String[temp.getCount()];
            for (int i = 0; i < temp.getCount(); i++) {
                temp.moveToPosition(i);
                objList[i] = temp.getString(0);
            }
        }
        return objList;
    }

    public static void CreateOrder(RestaurantObj obj) {
        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM");
        ContentValues gen = new ContentValues();
        gen.put("ResName", obj.getName());
        gen.put("SubTotal", obj.getSubTotal());
        gen.put("Total", obj.getTotal());
        gen.put("Date", dt1.format(new Date()));
        gen.put("Charges", obj.getCharges());
        gen.put("ChargesTotal", obj.getChargesOne());
        gen.put("ChargesPer", obj.getChargesPer());
        gen.put("ChargesTotalL", obj.getChargesOneL());
        gen.put("ChargesPerL", obj.getChargesPerL());
        db.insert("Orders", null, gen);

        Cursor temp = db.rawQuery("select max(OrderID) from Orders", null);
        temp.moveToFirst();
        int orderID = temp.getInt(0);

        for (int j = 0; j < obj.getExtraItems().size(); j++) {
            gen = new ContentValues();
            gen.put("ItemName", obj.getExtraItems().get(j).getName());
            gen.put("ItemType", obj.getExtraItems().get(j).getItem());
            gen.put("Total", obj.getExtraItems().get(j).getAmount());
            gen.put("CustomerID", -1);
            gen.put("OrderID", orderID);
            db.insert("Items", null, gen);
        }


        for (int i = 0; i < obj.getCustomer().size(); i++) {
            gen = new ContentValues();
            gen.put("CusName", obj.getCustomer().get(i).getName());
            gen.put("Total", obj.getCustomer().get(i).getTotal());
            gen.put("OrderID", orderID);
            db.insert("Customers", null, gen);

            temp = db.rawQuery("select max(CustomerID) from Customers", null);
            temp.moveToFirst();
            int customerID = temp.getInt(0);


            for (int j = 0; j < obj.getCustomer().get(i).getItems().size(); j++) {
                gen = new ContentValues();
                gen.put("ItemName", obj.getCustomer().get(i).getItems().get(j).getName());
                gen.put("ItemType", obj.getCustomer().get(i).getItems().get(j).getItem());
                gen.put("Total", obj.getCustomer().get(i).getItems().get(j).getAmount());
                gen.put("CustomerID", customerID);
                gen.put("OrderID", -1);
                db.insert("Items", null, gen);
            }
        }
        temp.close();
    }

    public static void UpdateOrder(int id, RestaurantObj obj) {
        db.execSQL("Delete From Items where CustomerID in (select CustomerID from Customers where OrderID =" + id + ")");
        db.delete("Customers", "OrderID = " + id + "", null);
        db.delete("Items", "OrderID = " + id + "", null);

        ContentValues gen = new ContentValues();
        gen.put("ResName", obj.getName());
        gen.put("SubTotal", obj.getSubTotal());
        gen.put("Total", obj.getTotal());
        gen.put("Charges", obj.getCharges());
        gen.put("ChargesTotal", obj.getChargesOne());
        gen.put("ChargesPer", obj.getChargesPer());
        gen.put("ChargesTotalL", obj.getChargesOneL());
        gen.put("ChargesPerL", obj.getChargesPerL());
        db.update("Orders", gen, "OrderID = ?", new String[]{String.valueOf(id)});

        Cursor temp = db.rawQuery("select max(OrderID) from Orders", null);
        temp.moveToFirst();
        int orderID = temp.getInt(0);


        for (int j = 0; j < obj.getExtraItems().size(); j++) {
            gen = new ContentValues();
            gen.put("ItemName", obj.getExtraItems().get(j).getName());
            gen.put("ItemType", obj.getExtraItems().get(j).getItem());
            gen.put("Total", obj.getExtraItems().get(j).getAmount());
            gen.put("CustomerID", -1);
            gen.put("OrderID", orderID);
            db.insert("Items", null, gen);
        }


        for (int i = 0; i < obj.getCustomer().size(); i++) {
            gen = new ContentValues();
            gen.put("CusName", obj.getCustomer().get(i).getName());
            gen.put("Total", obj.getCustomer().get(i).getTotal());
            gen.put("OrderID", orderID);
            db.insert("Customers", null, gen);

            temp = db.rawQuery("select max(CustomerID) from Customers", null);
            temp.moveToFirst();
            int customerID = temp.getInt(0);


            for (int j = 0; j < obj.getCustomer().get(i).getItems().size(); j++) {
                gen = new ContentValues();
                gen.put("ItemName", obj.getCustomer().get(i).getItems().get(j).getName());
                gen.put("Total", obj.getCustomer().get(i).getItems().get(j).getAmount());
                gen.put("ItemType", obj.getCustomer().get(i).getItems().get(j).getItem());
                gen.put("CustomerID", customerID);
                db.insert("Items", null, gen);
            }
        }
        temp.close();
    }

    public static void DeleteMeal(int id) {
        db.execSQL("Delete From Items where CustomerID in (select CustomerID from Customers where OrderID =" + id + ")");
        db.delete("Customers", "OrderID = " + id + "", null);
        db.delete("Orders", "OrderID = " + id + "", null);
        db.delete("Items", "OrderID = " + id + "", null);

    }
}
