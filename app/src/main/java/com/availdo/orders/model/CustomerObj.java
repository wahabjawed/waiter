package com.availdo.orders.model;

import java.util.List;

/**
 * Created by Wahab Jawed on 6/25/2016.
 */
public class CustomerObj {
    String name;
    double total;
    List<CustomerItemObj> items;
    String itemList;

    public CustomerObj(String name, double total, List<CustomerItemObj> items) {

        this.name = name;
        this.total = total;
        this.items = items;
        itemList="";
    }

    public String getItemList() {
        return itemList;
    }

    public void setItemList(String itemList) {
        this.itemList = itemList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<CustomerItemObj> getItems() {
        return items;
    }

    public void setItems(List<CustomerItemObj> items) {
        this.items = items;
    }
}
