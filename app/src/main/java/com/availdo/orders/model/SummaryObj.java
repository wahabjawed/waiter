package com.availdo.orders.model;

/**
 * Created by Wahab Jawed on 7/1/2016.
 */
public class SummaryObj {

    String particular;
    double price;
    int quantity;

    public SummaryObj(String particular, double price, int quantity) {
        this.particular = particular;
        this.price = price;
        this.quantity = quantity;
    }

    public String getParticular() {
        return particular;
    }

    public void setParticular(String particular) {
        this.particular = particular;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
