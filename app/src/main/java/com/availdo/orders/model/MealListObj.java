package com.availdo.orders.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by umer.altaf on 8/24/2015.
 */
public class MealListObj {
    private int id;
    private String name;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public MealListObj(int id, String name, String date) {
        this.id = id;
        this.name = name;
        this.date=date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

