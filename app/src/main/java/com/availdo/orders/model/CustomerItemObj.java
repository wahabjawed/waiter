package com.availdo.orders.model;

/**
 * Created by Wahab Jawed on 6/25/2016.
 */
public class CustomerItemObj {

    int id;
    String name;
    String item;
    double amount;

    public CustomerItemObj(int id, String name, double amount, String item) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.item=item;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
