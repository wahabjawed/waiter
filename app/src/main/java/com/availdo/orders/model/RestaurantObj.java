package com.availdo.orders.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wahab Jawed on 6/25/2016.
 */
public class RestaurantObj {

    String name;
    double subTotal;
    double charges;
    double chargesOne;
    double chargesPer;
    String chargesOneL;
    String chargesPerL;
    double total;

    public String getChargesOneL() {
        return chargesOneL;
    }

    public void setChargesOneL(String chargesOneL) {
        this.chargesOneL = chargesOneL;
    }

    public String getChargesPerL() {
        return chargesPerL;
    }

    public void setChargesPerL(String chargesPerL) {
        this.chargesPerL = chargesPerL;
    }

    List<CustomerObj> customer;

    List<CustomerItemObj> extraItems;

    public RestaurantObj() {
        this.name = "";
        this.subTotal = 0.00;
        this.charges = 0.00;
        this.chargesOne = 0.00;
        this.chargesPer = 0.00;
        this.total = 0.00;
        this.customer = new ArrayList<>();
        this.extraItems = new ArrayList<>();
    }

    public double getChargesOne() {
        return chargesOne;
    }

    public void setChargesOne(double chargesOne) {
        this.chargesOne = chargesOne;
    }

    public double getChargesPer() {
        return chargesPer;
    }

    public void setChargesPer(double chargesPer) {
        this.chargesPer = chargesPer;
    }

    public List<CustomerItemObj> getExtraItems() {
        return extraItems;
    }

    public void setExtraItems(List<CustomerItemObj> extraItems) {
        this.extraItems = extraItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getCharges() {
        return charges;
    }

    public void setCharges(double charges) {
        this.charges = charges;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<CustomerObj> getCustomer() {
        return customer;
    }

    public void setCustomer(List<CustomerObj> customer) {
        this.customer = customer;
    }
}
